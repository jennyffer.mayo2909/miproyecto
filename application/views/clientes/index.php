<br>
<center>
  <h2>LISTADO DE CLIENTES</h2>
</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/clientes/nuevo" class="btn btn-primary">
      <i class="fa fa-plus-circle"></i>
      Agregar Nuevo
    </a>
</center>

<?php if ($listadoClientes): ?>
  <table class="table" id="tbl-clientes">
        <thead>
            <tr>
              <th class="text-center">ID</th>
              <th class="text-center">FOTO</th>
              <th class="text-center">IDENTIFICACIÓN</th>
              <th class="text-center">APELLIDO</th>
              <th class="text-center">NOMBRE</th>
              <th class="text-center">ESTADO</th>
              <th class="text-center">PAIS</th>
              <th class="text-center">OPCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoClientes->result()
            as $filaTemporal): ?>
                  <tr>
                    <td class="text-center">
                      <?php echo $filaTemporal->id_cli; ?>
                    </td>
                    <td class="text-center">
                      <?php if ($filaTemporal->foto_cli!=""): ?>
                        <img
                        src="<?php echo base_url(); ?>/uploads/clientes/<?php echo $filaTemporal->foto_cli; ?>"
                        height="80px"
                        width="100px"
                        alt="">
                      <?php else: ?>
                        N/A
                      <?php endif; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $filaTemporal->identificacion_cli; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $filaTemporal->apellido_cli; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $filaTemporal->nombre_cli; ?>
                    </td>

                    <td class="text-center">
                      <?php if ($filaTemporal->estado_cli=="ACTIVO"): ?>
                        <div class="alert alert-success">
                          <?php echo $filaTemporal->estado_cli; ?>
                        </div>
                      <?php else: ?>
                        <div class="alert alert-danger">
                          <?php echo $filaTemporal->estado_cli; ?>
                        </div>
                      <?php endif; ?>
                    </td>
                    <td class="text-center">
                        <?php echo $filaTemporal->nombre_pais; ?>
                    </td>
                    <td class="text-center">
                        <a href="<?php echo site_url(); ?>/clientes/editar/<?php echo $filaTemporal->id_cli; ?>" class="btn btn-warning">
                          <i class="fa fa-pen"></i>
                        </a>

                        <?php if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR"): ?>
                          <a href="javascript:void(0)"
                           onclick="confirmarEliminacion('<?php echo $filaTemporal->id_cli; ?>');"
                           class="btn btn-danger">
                            <i class="fa fa-trash"></i>
                          </a>
                        <?php endif; ?>
                    </td>
                  </tr>
            <?php endforeach; ?>
        </tbody>
  </table>
<?php else: ?>
<div class="alert alert-danger">
    <h3>No se encontraron clientes registrados</h3>
</div>
<?php endif; ?>

<script type="text/javascript">
    function confirmarEliminacion(id_cli){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el cliente de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/clientes/procesarEliminacion/"+id_cli;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>

<script type="text/javascript">
    //Deber incorporar botones de EXPORTACION
    $("#tbl-clientes").DataTable();
</script>










<!--  -->
