<?php
    class Pais extends CI_Model{
      public function __construct(){
        parent::__construct();
      }

      public function consultarTodos(){
        $listadoPaises=$this->db->get("pais");
        if($listadoPaises->num_rows() >0){
          return $listadoPaises;
        }else{
          return false;
        }
      }

    }//cierre de la clase
